import 'package:flutter/material.dart';

class Product {
  final String name;
  final double price;

  Product({required this.name, required this.price});
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: ProductListScreen(),
    );
  }
}

class ProductListScreen extends StatelessWidget {
  final List<Product> products = [
    Product(name: 'Produto 1', price: 10.0),
    Product(name: 'Produto 2', price: 20.0),
    Product(name: 'Produto 3', price: 30.0),
    // Adicione mais produtos conforme necessário
  ];

  const ProductListScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de Produtos'),
      ),
      body: ListView.builder(
        itemCount: products.length,
        itemBuilder: (context, index) {
          final product = products[index];
          return GestureDetector(
            onTap: () {
              // Ação ao tocar no produto (exemplo: mostrar um diálogo)
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: const Text('Detalhes do Produto'),
                    content: Text(
                        'Nome: ${product.name}\nPreço: \$${product.price.toStringAsFixed(2)}'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop(); // Fechar o diálogo
                        },
                        child: const Text('Fechar'),
                      ),
                    ],
                  );
                },
              );
            },
            child: ListTile(
              title: Text(product.name),
              subtitle: Text('Preço: \$${product.price.toStringAsFixed(2)}'),
            ),
          );
        },
      ),
    );
  }
}
